#~nistmongoservice0/database/models.py
from .db import db

class Callstotexts(db.Document):
    sessionNumber = db.IntField(required=True)
    sourcePSA = db.StringField(required=True, unique=True)
    sessionStart = db.DateTimeField(required=True)
    callerCellPhone = db.StringField(required=True)
    agent = db.StringField(required=True, unique=True)
    idCallstoTexts = db.IntField(required=True)
    source = db.ListField(db.StringField(), required=True)
    timestamp = db.DateTimeField(required=True)
    messageText = db.StringField(required=True)
    latitude = db.DecimalField(required=True)
    longitude = db.DecimalField(required=True)

