#~/nistmongoservice0/app.py

from flask import Flask, jsonify,  request, Response
from database.db import initialize_db
from database.models import Callstotexts

app = Flask(__name__)


app.config['MONGODB_SETTINGS'] = {
    'host': 'mongodb://localhost/nistmongoservice0'
}

initialize_db(app)


@app.route('/nistcalltotext')
def get_nistcalltotext():
    callstotexts = Callstotexts.objects().to_json()
    return Response(callstotexts, mimetype="application/json", status=200)


@app.route('/nistcalltotext', methods=['POST'])
def add_nistcalltotext():
    body = request.get_json()
    callstotexts = Callstotexts(**body).save()
    id = callstotexts.id
    return {'id': str(id)}, 200

@app.route('/nistcalltotext/<id>', methods=['PUT'])
def update_nistcalltotext(id):
    body = request.get_json()
    Callstotexts.objects.get(id=id).update(**body)
    return '', 200

@app.route('/nistcalltotext/<id>', methods=['DELETE'])
def delete_nistcalltotext(id):
    Callstotexts.objects.get(id=id).delete()
    return '', 200

app.run()


